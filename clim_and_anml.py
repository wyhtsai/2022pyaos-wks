import xarray as xr 
import numpy as np
import pandas as pd
from scipy.fft import rfft, irfft

def clmDayTLL(data, multiindex=None):
    #### If multiindex == true, retain the original multiindex coordinates.
    #### If not, create another "day_of_year" coordinate.
    #### If calcDayAnom are about to be applied, multiindex == true is suggested. 
    grouper = xr.DataArray(
                        pd.MultiIndex.from_arrays(
                                                 [data.time.dt.month.values, data.time.dt.day.values],
                                                 names=['month', 'day']), 
                        dims=['time'], 
                        coords=[data.time],
                       )
    dataGB = data.groupby(grouper).mean()
    
    if (multiindex==True): 
       clmDay = dataGB 
    if (multiindex==False): 
        # convert to [day_of_year, lat, lon].
        nt, ny, nx = dataGB.shape
        clmDay = xr.DataArray(data=dataGB.values, 
                              dims=['time','lat','lon'],
                              coords=dict(day_of_year=range(1,nt+1),
                                          lat=dataGB.lat,
                                          lon=dataGB.lon)) 
    return(clmDay)

def smthClmDay(clmDay, nHarm):
    nt, ny, nx = clmDay.shape
    cf = rfft(clmDay.values, axis=0)     # xarray.DataArray.values 可將DataArray 轉換成numpy.ndarray。
    cf[nHarm,:,:] = 0.5*cf[nHarm,:,:]    # mini-taper.
    cf[nHarm+1:,:,:] = 0.0               # set all higher coef to 0.0
    icf = irfft(cf, n=nt, axis=0)       # reconstructed series
    clmDaySmth = clmDay.copy(data=icf, deep=False)
    return(clmDaySmth)

def calcDayAnom(data, daily_clim): 
    ### The climatology should be MultiIndex!! 
    grouper = xr.DataArray(
                        pd.MultiIndex.from_arrays(
                                                 [data.time.dt.month.values, data.time.dt.day.values],
                                                 names=['month', 'day']), 
                        dims=['time'], 
                        coords=[data.time],
                       )
    dataGB = data.groupby(grouper) 
    anml = data.copy(data=(dataGB - daily_clim), deep=False)
    
    return(anml)
    