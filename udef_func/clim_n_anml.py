import xarray as xr 
import numpy as np
import pandas as pd
from scipy.fft import rfft, irfft

def clmDayTLL(data):
    DayClm = data.groupby("time.dayofyear").mean("time")
    
    return(DayClm)

def smthClmDay(clmDay, nHarm):
    nt, ny, nx = clmDay.shape
    cf = rfft(clmDay.values, axis=0)     # xarray.DataArray.values 可將DataArray 轉換成numpy.ndarray。
    cf[nHarm,:,:] = 0.5*cf[nHarm,:,:]    # mini-taper.
    cf[nHarm+1:,:,:] = 0.0               # set all higher coef to 0.0
    icf = irfft(cf, n=nt, axis=0)       # reconstructed series
    clmDaySmth = clmDay.copy(data=icf, deep=False)
    return(clmDaySmth)

def calcDayAnom(data, daily_clim): 
    anml = data.copy(
                    data=(data.groupby('time.dayofyear') - daily_clim), 
                    deep=False)
    
    return(anml)