import numpy as np
import xarray as xr
import matplotlib as mpl
from matplotlib import pyplot as plt
from cartopy import crs as ccrs 
from cartopy import crs as ccrs   
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
mpl.rcParams['figure.dpi'] = 100

lats = -20
latn =  30
lon1 =  79 
lon2 = 161
time1 = '2017-12-01'
time2 = '2017-12-31'

olrm = (xr.open_dataset('data/olr.nc')
          .sel(lat=slice(lats,latn),lon=slice(lon1,lon2),time=slice(time1,time2))
          .olr
          .mean(dim='time'))
print(olrm)


# 繪圖：開啟繪圖空間與地圖設定。
proj = ccrs.PlateCarree()      # 採用等距地圖投影。
fig,ax = plt.subplots(1,1,subplot_kw={'projection':proj}) 
clevs = np.arange(170,350,20)

# 繪圖
plt.title("December 2017 mean OLR (W m$^{-2}$)", loc='left')    # 設定圖片標題，並且置於圖的左側。
olrPlot = (olrm.plot.contourf("lon",              # 設定x坐標。
                              "lat",              # 設定y坐標。
                              transform=proj,     # 指定「資料本身」的網格系統。
                              ax=ax,              # 繪製在繪圖空間ax上。
                              levels=clevs,       # 設定圖例色階間距。
                              cmap='rainbow',     # 設定色階
                              add_colorbar=True,  # 繪製色階。
                              extend='both',      # 色階向兩端延伸。
                              cbar_kwargs={'orientation': 'horizontal', 'aspect': 30, 'label': ' '}) #設定color bar
                              )

ax.set_extent([lon1,lon2,lats,latn],crs=proj)
ax.set_xticks(np.arange(80,180,20), crs=proj)
ax.set_yticks(np.arange(-20,40,10), crs=proj)   # 設定x, y座標的範圍，以及多少經緯度繪製刻度。
lon_formatter = LONGITUDE_FORMATTER
lat_formatter = LATITUDE_FORMATTER   
ax.xaxis.set_major_formatter(lon_formatter)
ax.yaxis.set_major_formatter(lat_formatter)  # 將經緯度以degN, degE的方式表示。
ax.coastlines()                # 繪製地圖海岸線。                              
ax.set_ylabel(' ')             # 設定坐標軸名稱。
ax.set_xlabel(' ')
#plt.show()
plt.savefig("olr_mean_201712.png", dpi=300)  # 儲存圖片。